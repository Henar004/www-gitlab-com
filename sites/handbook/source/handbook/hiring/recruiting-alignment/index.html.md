---
layout: markdown_page
title: "Recruiting Alignment"
---

## Search Team Alignment by Department

| Department                    | Recruiter       |  Coordinator | Sourcer     |
|--------------------------|-----------------|-----------------|-----------------|
| Executive          | Rich Kahn   | Kalene Godoy | Chriz Cruz |
| Enterprise Sales, NA | Marcus Carter | Corinne Sapolu |  Susan Hill |
| Enterprise Sales, EMEA | Debbie Harris |  Corinne Sapolu  |  Kanwal Matharu |
| Enterprise Sales, APAC | Debbie Harris |  Corinne Sapolu  |  Kanwal Matharu |
| Commercial Sales,	Global | Marcus Carter|  Corinne Sapolu| Susan Hill  | 
| Channel Sales, NA | Steph Sarff | Kalene Godoy |  J.D. Alex |
| Channel Sales, EMEA | Debbie Harris | Corinne Sapolu |  Kanwal Matharu |
| Channel Sales, APAC | Debbie Harris | Corinne Sapolu |  Kanwal Matharu |
| Field Operations,	NA | Steph Sarff |  Kalene Godoy | J.D. Alex | 
| Field Operations,	EMEA | Debbie Harris | Corinne Sapolu |  Kanwal Matharu |
| Field Operations,	APAC | Debbie Harris | Corinne Sapolu |  Kanwal Matharu |
| Customer Success, NA | Steph Sarff |  Kalene Godoy | J.D. Alex | 
| Customer Success, EMEA | Debbie Harris  | Corinne Sapolu | Kanwal Matharu |
| Customer Success, APAC | Debbie Harris  | Corinne Sapolu | Kanwal Matharu |
| Marketing, Global | Steph Sarff   | Kalene Godoy | J.D. Alex |
| G&A | Maria Gore | Ashley Jones | Loredana Iluca |
| Development | Rupert Douglas | Kalene Godoy  | Zsuzsanna Kovacs |
| Quality | Rupert Douglas   | Kalene Godoy  | Zsuzsanna Kovacs |
| UX  | Rupert Douglas   | Kalene Godoy  | Zsuzsanna Kovacs  |
| Support (EMEA) | Rupert Douglas  | Kalene Godoy  |  Joanna Michniewicz |
| Support (AMER & APAC) | Cyndi Walsh  | Ashley Jones  |  Joanna Michniewicz (AMER) / Zsuzsanna Kovacs (APAC)  |
| Security | Cyndi Walsh  | Ashley Jones  |  Zsuzsanna Kovacs |
| Infrastructure   | Matt Allen  | Corinne Sapulo | Chris Cruz |
| Product Management  | Matt Allen   | Corinne Sapulo | Chris Cruz |

## Recruiting Operations and Talent Branding Alignment

| Platform                    | Responsibility        | DRI     |
|--------------------------|-----------------|-----------------|
| Comparably | Admin  | Betsy Church and Erich Wegscheider |
| Comparably | Content Management | Betsy Church |
| Comparably | Reporting | Erich Wegscheider |
| Glassdoor | Admin  | Betsy Church and Erich Wegscheider |
| Glassdoor | Responding to Reviews  | Betsy Church |
| Glassdoor | Content Management | Betsy Church |
| Glassdoor | Reporting | Erich Wegscheider |
| LinkedIn | Admin - Recruiter  | Erich Wegscheider |
| LinkedIn | Seats | Erich Wegscheider |
| LinkedIn | Media - General | Marketing |
| LinkedIn | Media - Recruiting | Betsy Church |
| LinkedIn | Content Management | Marketing |
| LinkedIn | Content Management - Life at GitLab | Betsy Church |
| New Platform(s) | Requests | @domain |
| Recruitment Marketing  | Requests | @domain |
