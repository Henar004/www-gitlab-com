---
layout: handbook-page-toc
title: "Community advocacy guidelines"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

- [General](/handbook/marketing/community-relations/community-advocacy/guidelines/general)
