---
layout: solutions
title: GitLab on HashiCorp
description: "A complete DevOps platform to build, test, secure and deploy on HashiCorp Terraform"
suppress_header: true
extra_css:
  - bl-modal.css
---

***
{:.header-start}

# GitLab on HashiCorp
{:.header-left}

Standardize secrets managment and secure GitOps workflows with technologies from GitLab and HashiCorp.
{:.header-left}

<p class="header-left"><a class="btn cta-btn orange" href="/sales/">Talk to an expert</a> <span class="btn cta-btn ghost-purple bl-modal-open">See what we're doing at HashiConf</span></p>


***
{:.header-end}

> From security scanning, managing access to senstive data, enfocing policy and goverance or lowering the barrier of adoption into GitOps and Infrastructure-as-Code (IaC) across workflows - go from idea to production faster with GitLab and HashiCorp.

>  GitLab is a complete DevOps platform, delivered as a single application and with product integrations between HashiCorp Vault and Terraform; GitLab can simplfy workflows across both DevSecOps and DevOps teams.

***

## Joint HashiCorp and GitLab Benefits

GitLab collapses cycle times by driving higher efficiency across all stages of the software development lifecycle running on HashiCorp. 

* Standardize Secret Management across Dev and Prod.
    * Vault can be configured as the general secrets management provider for GitLab across delivery into Dev and rollout into production. When quickly spinning up secure development environments, users can install Vault into a Kubernetes clusters from within GitLab to better align with real production environments.  
    * Practitioners using GItLab CI/CD can easily authenticate via JWT and securely consume variables, secrets, and even service account credentials and then fetch and read secrets from Vault as necessary.
* Versioning and collaboration with GitLab SCM + Terraform
    * Provide a single source of truth for Developer and Cloud Infrastructure Admins with GitLab Source Code Management. Store your Terraform plans and trigger Terraform Enterprise and Terraform Cloud pipelines in Git repositories and treat Terraform plans and Sentinel policies in GitLab like developers treat application source code. Empower cross functional teams with visibility with direct access to collaborate and contribute towards infrastructure declared in code via code reviews, Git commits, Merge Requests, and overall transparent iteration via Git.
* Automated speed, reliability, and consistency with GitLab CI/CD + Terraform Enterprise
    * Provide flexible, template driven modular workflows via GitLab CI/CD that evoke Terraform plans for infrastructure life cycle management. Furthermore, view CI/CD and Terraform Plan outputs within GitLab MRs for contextual changes associated with each change. Additionally, Ops teams can also configure GitLab CI/CD to evoke stored Sentinel policies in Git repositories for Policy as Code framework that provides access control, security, and operational guard rails within the automated workflow.  
{:.list-benefits}

***

## Joint Solution Capabilities with HashiCorp

* ![Terraform logo](/images/solutions/hashicorp/Terraform_PrimaryLogo_Black_RGB.svg)
  * GitLab and Terraform provide developers, operators, and practitioners alike with the ability to manage infrastructure automation for complex, distributed applications while maintaining security, compliance, and reliability. Organizations standardizing on Terraform and GitLab can simplify their technology stack for software and infrastructure lifecycle management and empower their engineers to collaborate, automate, and secure processes across multi-cloud environments. 
e. **[[Learn More]](https://www.terraform.io/)**
* ![Vault logo](/images/solutions/hashicorp/Vault_PrimaryLogo_Black_RGB.svg)
  * Vault is a single security control plane for operations and infrastructure that many organizations have chosen to manage ACL, secrets and other sensitive data. Additionally, GitLab is a complete DevOps platform delivered as a single application for the SDLC process. Together, this joint solution provides an alternative to the error-prone, document-based collaboration methods used across teams and allows organization to migrate from bolting on security to shifting security left and can be configured together to standardize the security of secrets, tokens, credentials, etc. across development and production. 
. **[[Learn More]](https://www.vaultproject.io/)**
{:.list-capabilities}

---

## GitLab and HashiCorp Joint Solutions in Action
{:.no-color}

- [GitOps:The Future of Infrastructure Automation - A panel discussion with Weaveworks, HashiCorp, Red Hat, and GitLab](https://about.gitlab.com/why/gitops-infrastructure-automation/)
- [GitLab and HashiCorp: Providing application and infrastructure delivery workflows](https://about.gitlab.com/blog/2019/09/17/gitlab-hashicorp-terraform-vault-pt-1/)
- [How Wag! cut their release process from 40 minutes to just 6](https://about.gitlab.com/blog/2019/01/16/wag-labs-blog-post/)
- [GitLab and HashiCorp - A holistic guide to GitOps and the Cloud Operating Model](https://about.gitlab.com/webcast/gitlab-hashicorp-gitops/)
- [Empowering Developers and Operators through GitLab and HashiCorp](https://www.hashicorp.com/resources/empowering-developers-and-operators-through-gitlab-and-hashicorp/) 
- [HashiCorp Vault and GitLab integration: Why and How?](https://www.youtube.com/watch?v=VmQZwfgp3aA)

{:.list-resources}

---

<div class="bl-modal" id="solutions-modal">
  <img class="bl-modal-close" alt="close the popup" src="/images/icons/x.svg" />
  <div class="bl-modal-content">
    <h2>We're at HashiConf Digital, October 12-15, 2020!</h2>
    <p>Join us at one of our Lightning Talks to learn more about GitLab + HashiCorp or one of our virtual happy hours, Git Happy with GitLab!</p>
    <h3>Oct 14, 2020</h3>
    <dl>
      <dt>Using Vault secrets in GitLab CI | 2:35-2:50pm Pacific</dt>
      <dd>GitLab wants to make it easy for businesses to adopt modern secrets management solutions like Hashicorp Vault in their Software delivery life cycle. Join us to learn how to configure Vault to allow GitLab CI to authenticate and read secrets within pipelines. In this talk, you will see an overview of the new CI secrets syntax available in GitLab 13.4 and what it means for GitLab end users. <em>Krasimir Angelov, Backend Engineer at GitLab</em></dd>
    </dl>
    <dl>
      <dt>Git Happy with GitLab, Virtual Happy Hour | 3:30-5:00pm Pacific</dt>
      <dd>Join GitLab for a Virtual Happy Hour during HashiConf. The happy hour will run in a speed networking format that enables everyone to connect with each other through multiple rounds of five-minute, one-on-one conversations. Meet and greet with other HashiConf attendees, GitLab Team Members and more! <a href="https://www.runtheworld.today/app/invitation/8901"><strong>[ RSVP ]</strong></a></dd>
    </dl>
    <h3>Oct 15, 2020</h3>
    <dl>
      <dt>GitOps with Terraform Cloud and GitLab | 2:00-2:15pm Pacific</dt>
      <dd>GitOps brings a lot of power to DevOps practitioners but “With great power comes great responsibility.” See how DevOps cross functional groups within organizations looking in increase velocity are empowered through flexible workflow with Terraform Cloud and GitLab. Learn more about best practices and features that allow your team to focus more on managing infrastructure, and less on security concerns. <em>Brad Downey, Strategic Account Leader at GitLab</em></dd>
    </dl>
    <dl>
      <dt>Git Happy with GitLab, Virtual Happy Hour | 3:30-5:00pm Pacific</dt>
      <dd>Join GitLab for a Virtual Happy Hour during HashiConf. The happy hour will run in a speed networking format that enables everyone to connect with each other through multiple rounds of five-minute, one-on-one conversations. Meet and greet with other HashiConf attendees, GitLab Team Members and more! <a href="https://www.runtheworld.today/app/invitation/8901"><strong>[ RSVP ]</strong></a></dd>
    </dl>
  </div>
</div>

<script src="/javascripts/bl-modal.js" type="text/javascript" data-cookieconsent="ignore"></script>
<script type="text/javascript" data-cookieconsent="ignore">
  blOpenModal('.bl-modal-open', '#solutions-modal');
</script>
<style>
  .bl-modal.bl-visible {
    width: 900px !important;
  }
</style>
