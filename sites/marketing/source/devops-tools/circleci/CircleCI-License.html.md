---
layout: markdown_page
title: "CircleCI License Overview"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

<!------------------Begin page additions below this line ------------------ -->

## Pricing Plans

* Free ($0)
   - 2,500 free credits/week
   - Run 1 job at a time
   - Build on Linux or Window (no macOS support)
   - Does not support flexible payment option (credit card or invoice)
   - Support Options:
      - Community
* Performance (starting at $30/month)
   - $15/month for the first 3 users and then $15/month for each added user
   - Starts at 25,000 credits for $15
   - Does not support flexible payment option (credit card or invoice)
   - Support Options:
      - Community
      - Support Portal
      - Global Ticket Support
      - 8×5 SLAs available
      - Account Team: Customer Success Manager 
* Custom
   - Plan customized for the customer
   - Support Options:
      - Community
      - Support Portal
      - Global Ticket Support
      - 24×5 and 24×7 SLAs available
      - Account Team: Customer Success Manager, Customer Success Engineer, Implementation Manager
